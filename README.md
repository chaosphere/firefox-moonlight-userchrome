# Moonlight userChrome theme for firefox

In this repo you will find my personal take on userChrome customizations for Firefox with the Moonlight theme.

This theme is inspired by the [Moonlight vscode theme](https://marketplace.visualstudio.com/items?itemName=atomiks.moonlight)
and [Moonlight theme for github](https://github.com/Brettm12345/github-moonlight).

If you need a more complete and accurate theme for Firefox with better support etc, I encurage you to
take a look at this [Moonlight userChrome](https://github.com/eduardhojbota/moonlight-userChrome) theme.

# How to use/install

- Step 1:
    * In the URL bar type: __about:profiles__
    * Look for the profile which has the "Default Profile" property set to true
    * Click on "Open Folder" button belonging to the "Root Directory" property

- Step 2:
    * Create a folder and named it _chrome_ if it does not already exist
    * Copy the __userChrome.css__ file from this repo into the _chrome_ folder or just copy paste the 
    content of the file into your preexistent _userChrome_ file

- Step 3 for Firefox v69+:
    * In the URL bar type: __about:config__
    * Accept the "risk" and continue
    * Search for the string: __toolkit.legacyUserProfileCustomizations.stylesheets__
    * Double click the string in the result, setting it to __true__
    * Restart Firefox to see the theme being applied

# Disclaimer

This is my personal take on the theme and it suits well my needs. Contributions are welcomed but bare in mind
that this is just a __personal__ project that does not want to be professional neither complete. 
